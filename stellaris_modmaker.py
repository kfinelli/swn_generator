import json
import random
import sys
import os

#scaling from hex map to stellaris galaxy coords
coordscalex=-25
coordscaley=25

'''TODO list (priority order):
    researchBase
    refuelingStation
    gasGiantMine
    asteroidBase
    moonBase
    planet improvements (population)
    spaceStation in asteroidBelt
    '''

#hard-coded settings, unlikely to need changing now
descriptor = '''version="1.0"
tags={
    "Galaxy Generation"
    }
    name="SWN map"
    path="mod/swnmap"
    supported_version="2.7.*"
'''

static_galaxy_header = '''#place in map/setup_scenarios/swn_map.txt
static_galaxy_scenario = {
    name = "SWN Sector"
    priority = 1
    default = yes
    core_radius = 0
    num_empires = { min = 0 max = 4 }
    num_empire_default = 2
    fallen_empire_default = 0
    fallen_empire_max = 0
    crisis_strength = 1
    primitive_odds = 1
    num_gateways_default = 0
    num_wormhole_pairs_default = 0
    advanced_empire_default = 0
    colonizable_planet_odds = 1.0
    random_hyperlanes = no
    marauder_empire_default = 0
    marauder_empire_max = 0
    num_wormhole_pairs = { min = 0 max = 0 }
    num_wormhole_pairs_default = 0
    num_gateways = { min = 0 max = 0 }
    num_gateways_default = 0
'''
#redundant, but in there in case we need it
ship_designs = '''#place in common/global_ship_designs/swn_ship_designs.txt
# deep space station
ship_design = {
    name = "Deep Space Station"
    ship_size = primitive_space_station

    section = {
        template = "OUTPOST_STARBASE_SECTION"
        slot = "mid"
    }
    required_component = "CORVETTE_FISSION_REACTOR"
}

ship_design = {
    name = "Space Station"
    ship_size = primitive_space_station

    section = {
        template = "OUTPOST_STARBASE_SECTION"
        slot = "mid"
    }
    required_component = "CORVETTE_FISSION_REACTOR"
}'''

##curly brackets are escaped for string formatting
prescripted_empires='''#place in prescripted_empires/swn_humans.txt
        humans1 = {{
    name = "humans1"
    adjective = "Human"
    spawn_enabled = yes # yes / no / always
    ignore_portrait_duplication = yes
    
    ship_prefix = "SS"
    
    species = {{
        class = "HUM"
        portrait = "human"
        name = "PRESCRIPTED_species_name_humans1"
        plural = "PRESCRIPTED_species_plural_humans1"
        adjective = "PRESCRIPTED_species_adjective_humans1"
        name_list = "HUMAN1"
        trait = "trait_adaptive"
        trait = "trait_nomadic"
        trait = "trait_wasteful"
    }}
    
    room = "personality_federation_builders_room"
    
    authority = "auth_democratic"
    civics = {{ "civic_beacon_of_liberty" "civic_idealistic_foundation" }}
    government = gov_representative_democracy
    
    ethic = "ethic_xenophile"
    ethic = "ethic_fanatic_egalitarian"

    origin = "origin_default"

    flags = {{ human_1 custom_start_screen }}
    
    planet_class = "pc_continental"
        planet_name = "{homeworld}"
        system_name = "{home_system}"
        initializer = "{home_system_initializer}_system_initializer"

    graphical_culture = "humanoid_01"
    city_graphical_culture = "humanoid_01"
    
    empire_flag = {{
        icon= {{
            category = "human"
            file = "flag_human_9.dds"
        }}
        background= {{
            category = "backgrounds"
            file = "00_solid.dds"
        }}
        colors={{
            "blue"
            "black"
            "null"
            "null"
        }}
    }}
    
    ruler = {{
        name = "PRESCRIPTED_ruler_name_humans1"
        gender = female
        portrait = "human_female_05"
        texture = 0
        hair = 1
        clothes = 0
        leader_class = ruler
    }}
}} '''

#hex math from redblobgames:
oddq_directions = [
        [[+1, +1], [+1,  0], [ 0, -1], 
            [-1,  0], [-1, +1], [ 0, +1]],
        [[+1,  0], [+1, -1], [ 0, -1], 
            [-1, -1], [-1,  0], [ 0, +1]],
    ]


#string manipulation
def readable_codename(s):
    return s.lower().replace(' ','_').replace('-','_')

#translate SWN types into Stellaris types
def star_class(attr):
    x = random.random()
    if x < 0.03: return "sc_b"
    if x < 0.06: return "sc_a"
    if x < 0.18: return "sc_f"
    if x < 0.40: return "sc_g"
    if x < 0.70: return "sc_k"
    if x < 0.71: return "sc_m_giant"
    if x < 0.72: return "sc_t"
    return "sc_m"

def planet_ring(attr):
    '''stub'''
    if random.random()<0.1: return "yes"
    else: return "no"

def planet_size(attr):
    '''generate randomly, not much else to go on'''
    return random.randrange(8,40)

def planet_class(attr):
    '''TODO: interpret planet tags like Desert world, ocean world, urbanized surface'''
    plist = []
    if attr['atmosphere']=='Airless or thin atmosphere':
        plist = plist+["pc_barren"]
    if attr['atmosphere']=='Corrosive' or attr['atmosphere']=='Corrosive and invasive atmosphere' or attr['atmosphere']=='Invasive, toxic atmosphere':
        plist = plist+["pc_toxic"]
    if attr['temperature']=='Frozen':plist = plist + ["pc_barren_cold", "pc_frozen"]
    if attr['temperature']=='Cold':  plist = plist +["pc_tundra", "pc_arctic", "pc_alpine"]
    if attr['temperature']=='Variable cold-to-temperate': plist = plist +["pc_tundra", "pc_alpine", "pc_ocean"]
    if attr['temperature']=='Temperate': plist = plist +["pc_tropical","pc_continental","pc_ocean"]
    if attr['temperature']=='Variable temperate-to-warm': plist = plist +["pc_arid","pc_savannah","pc_tropical"]
    if attr['temperature']=='Warm': plist = plist +["pc_desert","pc_arid","pc_savannah"]
    if attr['temperature']=='Burning': plist = ["pc_molten"]
    return random.choice(plist)

def moon_class():
    '''return a random moon class'''
    classes =  ["pc_barren", "pc_barren_cold", "pc_frozen", "pc_molten", "pc_toxic"]
    return random.choice(classes)

#json access
def get_objects_dict(data, parentId, category):
    rdict = {}
    for rid,robj in data[category].items():
        if robj['parent']==parentId:
            rdict[rid] = robj
    return rdict


def get_misc_single_object(data, sysid, category):
    for obj in data[category].values():
        if obj['parent']==sysid:
            return obj
    return 0

#hex map math
def is_neighbor(sys1, sys2):
    x1 = sys1['x']
    x2 = sys2['x']
    y1 = sys1['y']
    y2 = sys2['y']
    parity = x1 % 2
    for direction in range(6):
        hdir = oddq_directions[parity][direction]
        if (x2 == x1+hdir[0] and y2 == y1+hdir[1]): return True
    return False

#writers
#TODO: move more writers into functions here
def add_orbitalRuins(of, ruinsdict):
            for stid,station in ruinsdict.items():
                of.write('       init_effect = {\n')
                of.write('         create_fleet = {\n')
                of.write('           name = "Orbital vault" \n')
                of.write('             effect = {\n')
                of.write('               set_owner = last_created_country\n')
                of.write('                 create_ship = {\n')
                of.write('                   name = "Orbital vault"\n')
                of.write('                     graphical_culture = "ancient"\n')
                of.write('                     design = "NAME_Ancient_Sentinel"\n')
                of.write('                 }\n')
                of.write('               set_location = PREV \n')
                of.write('             }\n')
                of.write('         }\n')
                of.write('    }\n')

#start
def main(input_filename):
    data = {}
    with open(input_filename, 'r', encoding='utf-8') as in_file:
        data = json.load(in_file)

    #Union of black holes and stars are all going to become systems in Stellaris
    maplevel_objects = dict(data['system'], **data['blackHole'])
    #stellaris requires a home system to spawn an empire
    home_system_id = random.choice(list(data['system'].keys()))
    home_planet_id = random.choice(list(get_objects_dict(data, home_system_id, 'planet')))

    #ready to start, make empty directories
    for dirname in ['swnmap/map/galaxy/', 'swnmap/map/setup_scenarios',
            'swnmap/prescripted_countries', 'swnmap/events', 'swnmap/common/global_ship_designs',
            'swnmap/common/on_actions',
            'swnmap/common/solar_system_initializers']:
        if not os.path.exists(dirname):
                os.makedirs(dirname)

    with open('swnmap/map/setup_scenarios/swn_map.txt', 'w') as out_file:
        hyperlanes = set()
        out_file.write(static_galaxy_header)
        for sysid, system in maplevel_objects.items():
            sysx = system['x']
            sysy = system['y']
            initializer = ''
            if sysid in data['blackHole']: initializer = 'initializer = special_init_01 '
            else:
                initializer = 'initializer = {}_system_initializer'.format(readable_codename(system['name']))
            out_file.write('system = {{ id = "{idno}" name = "{name}" position = {{ x = {posx} y = {posy} }} {initializer}'.format(
                idno=sysid, name=system['name'], posx=sysx*coordscalex, posy=sysy*coordscaley, initializer=initializer))
            #add spawn weight for starting system
            if home_system_id == sysid:
                out_file.write('\n spawn_weight = { base = 0 modifier = { add = 10 has_country_flag = human_1 } } }\n')
            else:
                out_file.write('}\n')
            neighbors= [nbr for nbr in maplevel_objects.keys() if is_neighbor(maplevel_objects[nbr], system)]
            for k in neighbors: hyperlanes.add(frozenset([sysid,k]))
        for lanepair in hyperlanes:
            lanes = list(lanepair)
            out_file.write('add_hyperlane = {{ from = "{}" to = "{}" }}\n'.format(lanes[0], lanes[1]))
        out_file.write('}\n')


    with open('swnmap/common/solar_system_initializers/system_initializers.txt', 'w') as out_file:
        out_file.write('@base_moon_distance=10\n')
        for sysid, system in data['system'].items():
            planets = get_objects_dict(data, sysid, 'planet')
            usage = 'misc_system_init'
            if sysid==home_system_id: usage='custom_empire'
            out_file.write('{}_system_initializer = {{\n'.format(readable_codename(system['name'])))
            out_file.write('    name = "{}"\n'.format(system['name']))
            out_file.write('    class = "{}"\n'.format(star_class(system)))
            out_file.write('    usage = {}\n'.format(usage))
            out_file.write('    usage_odds = 0\n')
            out_file.write('    max_instances = 1\n\n')

            #asteroid belt
            if get_misc_single_object(data, sysid, 'asteroidBelt'):
                atype = "rocky_asteroid_belt"
                if random.random()<0.2: atype = "icy_asteroid_belt"
                out_file.write('    asteroid_belt = {\n')
                out_file.write('        type = {}\n'.format(atype))
                out_file.write('        radius = 70\n')
                out_file.write('    }\n\n')


            #first "planet" is the star
            out_file.write('    planet = {\n')
            out_file.write('        name = "{}"\n'.format(system['name']))
            out_file.write('        class = star\n')
            out_file.write('        orbit_distance = 0\n')
            out_file.write('        orbit_angle = 1\n')
            out_file.write('        size = 20\n')
            #deepSpaceStation
            if get_misc_single_object(data, sysid, 'deepSpaceStation'):
                #starbases don't seem to work well...
               # out_file.write('    init_effect = {\n')
               # out_file.write('        create_mandate_country = yes\n')
               # out_file.write('        create_starbase = {\n')
               # out_file.write('            size = starbase_outpost\n')
               # out_file.write('            owner = event_target:mandate_country\n')
               # out_file.write('        }\n')
               # out_file.write('    }\n')
                    #...so just use fleets/ships
                    out_file.write('    init_effect = {\n')
                    out_file.write('        create_fleet = {\n')
                    out_file.write('            name = "{} Station" \n'.format(system['name']))
                    out_file.write('            effect = {\n')
                    out_file.write('                set_owner = last_created_country\n')
                    out_file.write('                create_ship = {\n')
                    out_file.write('                    name = "{} Station"\n'.format(system['name']))
                    out_file.write('                    design = "Deep Space Station"\n')
                    out_file.write('                }\n')
                    out_file.write('                set_location = {\n')
                    out_file.write('                    target = PREV\n')
                    out_file.write('                    distance = 80\n')
                    out_file.write('                    angle = random\n')
                    out_file.write('                }\n')
                    out_file.write('            }\n')
                    out_file.write('        }\n')
                    out_file.write('    }\n')

            out_file.write('    }\n\n')

            out_file.write('    change_orbit = 25\n')
            for pid,planet in planets.items():
                out_file.write('    planet = {\n')
                out_file.write('        name = "{}"\n'.format(planet['name']))
                out_file.write('        class = "{}"\n'.format(planet_class(planet['attributes'])))
                out_file.write('        orbit_distance = {}\n'.format(random.randrange(15,60)))
                out_file.write('        orbit_angle = {}\n'.format(random.randrange(20,270)))
                out_file.write('        size = {}\n'.format(planet_size(planet)))
                out_file.write('        has_ring = "{}"\n'.format(planet_ring(planet)))
                moons = get_objects_dict(data, pid, 'moon')
                if moons: 
                    out_file.write('        change_orbit = @base_moon_distance\n')
                for mid,moon in moons.items():
                    out_file.write('        moon = {\n')
                    out_file.write('        name = "{}"\n'.format(moon['name']))
                    out_file.write('        class = "{}"\n'.format(moon_class()))
                    out_file.write('        orbit_distance = 5\n')
                    out_file.write('        orbit_angle = {}\n'.format(random.randrange(90,270)))

                    #orbitalRuin
                    orbitalRuins = get_objects_dict(data, mid, 'orbitalRuin')
                    add_orbitalRuins(out_file, orbitalRuins)
                    out_file.write('        }\n\n')

                #spaceStation
                #mining stations on planets act weird, use spaceStation ship instead
                spaceStations = get_objects_dict(data, pid, 'spaceStation')
                for stid,station in spaceStations.items():
                    out_file.write('    init_effect = {\n')
                    out_file.write('        create_fleet = {\n')
                    out_file.write('            name = "{}" \n'.format(station['name']))
                    out_file.write('            effect = {\n')
                    out_file.write('                set_owner = last_created_country\n')
                    out_file.write('                create_ship = {\n')
                    out_file.write('                    name = "{}"\n'.format(station['name']))
                    out_file.write('                    design = "Space Station"\n')
                    out_file.write('                }\n')
                    out_file.write('                set_location = PREV\n')
                    out_file.write('            }\n')
                    out_file.write('        }\n')
                    out_file.write('    }\n')
                #orbitalRuin
                orbitalRuins = get_objects_dict(data, pid, 'orbitalRuin')
                add_orbitalRuins(out_file, orbitalRuins)

                out_file.write('    }\n\n')
            out_file.write('}\n\n')


    #misc scripting and other stuff
    with open('swnmap/common/global_ship_designs/swn_ship_designs.txt','w') as out_file:
        out_file.write(ship_designs)
    with open('swnmap/prescripted_countries/swn_humans.txt','w') as out_file:
        out_file.write(prescripted_empires.format(homeworld = data['planet'][home_planet_id]['name'],
            home_system = data['system'][home_system_id]['name'],
            home_system_initializer = readable_codename(data['system'][home_system_id]['name'])))

    with open('swnmap.mod','w') as out_file:
        out_file.write(descriptor)
    with open('swnmap/descriptor.mod','w') as out_file:
        out_file.write(descriptor)
    #blanks (to hide base game scripting)
    with open('swnmap/common/on_actions/00_on_actions.txt','w') as out_file:
        out_file.write('\n')
    for x in ['base.txt', 'elliptical.txt', 'ring.txt', 'spiral_2.txt',
            'spiral_4.txt', 'static.txt']:
        with open('swnmap/map/galaxy/{}'.format(x),'w') as out_file:
            out_file.write('\n')
    for x in ['huge.txt','large.txt','medium.txt','small.txt','tiny.txt']:
        with open('swnmap/map/setup_scenarios/{}'.format(x),'w') as out_file:
            out_file.write('\n')
    for x in ['00_top_countries.txt', '88_megacorp_prescripted_countries.txt',
            '89_humanoids_prescripted_countries.txt',
            '90_syndaw_prescripted_countries.txt',
            '91_utopia_prescripted_countries.txt',
            '92_plantoids_prescripted_countries.txt',
            '93_lithoids_prescripted_countries.txt',
            '99_prescripted_countries.txt']:
        with open('swnmap/prescripted_countries/{}'.format(x),'w') as out_file:
            out_file.write('\n')

if __name__ == "__main__":
    main(sys.argv[1])

