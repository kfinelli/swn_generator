**How to use Stellaris mod generator:**

Create your sector using Sectors Without Number ([sectorswithoutnumber.com](https://sectorswithoutnumber.com/)).
Export the entire sector as json by clicking on sector view, clicking the import
button, then selecting json as the format.

Run the script by doing `python stellaris_modmaker.py my_sector.json` where
`my_sector.json` is the file you just downloaded from Sectors Without Number.

Add your mod to Stellaris by copying the new folder to the appropriate directory
(system dependent, usually under `Paradox Plaza/Stellaris/mods`). You may wish
to personalize the mod name by editing `swnmap.mod` and `descriptor.mod`. Load
the mod using the Add Mod button in the Stellaris launcher.

When running the game, it's best to set up a single player game with 0 AI
players, and choose your Empire as the pre-scripted Humans. This will ensure
nothing in your mod is over-written by a custom empire start.

**Random NPC name generator:**

Random generators from tables in Stars Without Number by Kevin Crawford. Uses the "random name" tables from p239 of the free revised edition pdf.

Additional tables taken by hand from lists at www.behindthename.com (Swahili, Korean, etc)