#!/usr/bin/env python

import json
from random import choice

def people():
    data = {}
    input_filename = 'lists/people.json'
    with open(input_filename, 'r', encoding='utf-8') as in_file:
        data = json.load(in_file)
    #print(data['Money']['overall situation'][2])
    #simply pick one thing from each list
    motivation = choice(data['motivation'])
    want = choice(data['want'])
    power = choice(data['power'])
    hook = choice(data['hook'])
    manner = choice(data['manner'])
    outcome = choice(data['default outcome'])
    return ('Their motivation: {}\n'
    'Their want: {}\n'
    'Their power: {}\n'
    'Their hook: {}\n'
    'Initial manner: {}\n'
    'Default deal outcome: {}'.format(motivation, want, power, hook, manner, outcome))

if __name__ == "__main__":
    print(people())

