import os

import discord
from dotenv import load_dotenv
from discord.ext import commands
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

from gen_name import gen_name
from people import people
from problems import problems

bot = commands.Bot(command_prefix='!')

@bot.command(name='test')
async def test(ctx):
    print('test message received')
    response = 'testing'
    await ctx.send(response)

@bot.command(name='swn_name')
async def swn_name(ctx):
    print('gen_name called')
    response = gen_name()
    await ctx.send(response)

@bot.command(name='swn_people')
async def swn_people(ctx):
    print('people called')
    response = people()
    await ctx.send(response)

@bot.command(name='swn_problems')
async def swn_problems(ctx):
    print('problems called')
    response = problems()
    await ctx.send(response)

bot.run(TOKEN)
