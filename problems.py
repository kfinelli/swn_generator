#!/usr/bin/env python

import json
from random import choice

def problems(printout=False):
    data = {}
    input_filename = 'lists/problems.json'
    with open(input_filename, 'r', encoding='utf-8') as in_file:
        data = json.load(in_file)
    #print(data['Money']['overall situation'][2])
    #pick conflict type
    conflict = choice(list(data['conflict type'].keys()))
    situation = choice(data['conflict type'][conflict]['overall situation'])
    focus = choice(data['conflict type'][conflict]['specific focus'])
    restraint = choice(data['restraint'])
    twist = choice (data['twist'])
    output = """Conflict type: {}
Overall situation: {}
Specific focus: {}
The restraint: {}
The twist: {}""".format(conflict, situation, focus, restraint, twist)
    if printout: print(output)
    return (output)

if __name__ == "__main__":
    problems(printout=True)

