#!/usr/bin/env python

from glob import glob
from random import choice

malenames_files = glob('lists/names_*_male*txt')
femalenames_files = glob('lists/names_*_female*txt')
surnames_files = glob('lists/names_*_surname*txt')

def fill_name_list(files):
    outlist = {}
    for currentfile in files:
        with open(currentfile) as f:
            temp = f.read().splitlines()
            listname = currentfile.split('_')[1]
            outlist[listname] = temp
    return outlist


malenames = fill_name_list(malenames_files)
femalenames = fill_name_list(femalenames_files)
surnames = fill_name_list(surnames_files)

gendernames = {'M':malenames, 'F':femalenames}

def gen_name():
    gender = choice(list(gendernames))
    culture_primary = choice(list(gendernames[gender]))
    culture_secondary = choice(list(surnames))

    return '%s: %s %s [%s, %s]'%(gender,
            choice(gendernames[gender][culture_primary]),
            choice(surnames[culture_secondary]),
            culture_primary, culture_secondary)

if __name__ == "__main__":
    print(gen_name())
